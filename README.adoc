= Curate
:toc:

[.lead]
Perform routine tasks to keep GitLab groups and projects tidy.

== Overview

On a schedule, this code will:

- find all GitLab projects visible to the access token
- check for a `.curate.yaml` or `.curate.yml` file
- cleanup pipeline logs based on desired retention policy

== Requirements

=== GitLab group access token

At a GitLab group level somewhere in the heirarchy above the GitLab projects you want to curate, 
create a group level access token with the following scopes:

* read_api
* read_repository
* write_repository

=== GitLab CI/CD variable

In this Gitlab project, create a CI/CD Variable named `GITLAB_ACCESS_TOKEN` with the value of the access token.

=== GitLab pipline schedule

=== Opt-in on an individual GitLab project basis

Add the file `.curate.yaml` or `.curate.yml` to the top level directory of your GitLab project with the following content:

[source,yaml]
----
---
cleanup_pipeline_logs:
  minimum_to_retain: 10
  retention_period_in_days: 14
----

In the example above, pipeline logs will be retained for at least 14 days with at least 10 logs retained regardless of age.

=== Opt-in on a goup level

Add the file `.curate.yaml` or `.curate.yml` to the top level directory of your GitLab group's README project.

== Features

* List and manage inactive projects
* Archive projects that have been inactive for a specified period
* Delete projects that have been archived for a specified period
* Generate reports on project activity and storage usage
* Manage group and project labels

== Installation

To install Curate, follow these steps:

1. Clone the repository:
+
[source,bash]
----
git clone https://gitlab.com/your-username/curate.git
cd curate
----

2. Install dependencies:
+
[source,bash]
----
pip install -r requirements.txt
----

== Usage

To use Curate, run the following command:

[source,bash]
----
python curate.py [options]
----

For a list of available options, use the `--help` flag:

[source,bash]
----
python curate.py --help
----

== Configuration

Create a `config.yaml` file in the project root directory with the following structure:

[source,yaml]
----
gitlab:
  url: 'https://gitlab.com'
  token: 'your_personal_access_token'

settings:
  inactive_days: 90
  archive_days: 180
  delete_days: 365
----

Replace `'your_personal_access_token'` with your actual GitLab personal access token.

== Contributing

We welcome contributions to Curate! Please see our [CONTRIBUTING.md](CONTRIBUTING.md) file for details on how to contribute.

== License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
