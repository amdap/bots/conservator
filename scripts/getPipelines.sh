#!/usr/bin/env bash

getPipelines() {

  ## Global variables required
  #
  # TOKEN
  # PROJECT
  # PER_PAGE
  # GRAPHQL_ENDPOINT

  ## Global functions required
  #
  # GetCreds

  local AFTER QUERY

  read -r -d '' QUERY <<'EOF'
query($fullPath: ID!, $first: Int, $after: String) {
  project(fullPath: $fullPath) {
    id
    pipelines(first: $first, after: $after) {
      count
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          active
          id
          updatedAt
        }
      }
    }
  }
}
EOF

  AFTER=null

  while :; do

    local VARIABLES PAYLOAD RESPONSE HAS_NEXT_PAGE END_CURSOR

    read -r -d '' VARIABLES <<EOF
{
  "fullPath": "${PROJECT}",
  "first": ${PER_PAGE},
  "after": ${AFTER}
}
EOF

    PAYLOAD=$(
      jq --null-input --arg query "$QUERY" --argjson variables "$VARIABLES" \
        '{"query": $query, "variables": $variables}'
    )

    RESPONSE=$(
      getCreds |
        curl --request POST --fail --silent --show-error --location \
          --header "Content-Type: application/json; charset=utf-8" \
          --config - "${GRAPHQL_ENDPOINT}" --data "$PAYLOAD"
    )

    HAS_NEXT_PAGE=$(echo "$RESPONSE" | jq -r '.data.project.pipelines.pageInfo.hasNextPage')
    END_CURSOR=$(echo "$RESPONSE" | jq -r '.data.project.pipelines.pageInfo.endCursor')
    echo "$RESPONSE" | jq -r '.data.project.pipelines.edges[] | @json'

    if [[ "$HAS_NEXT_PAGE" == 'false' ]]; then break; fi

    read -r -d '' AFTER <<EOF
"${END_CURSOR}"
EOF

  done

}
