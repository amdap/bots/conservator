#!/usr/bin/env bash

set -e  # Exit immediately if a command exits with a non-zero status
set -o pipefail  # Ensure pipeline errors are caught
exec 2>&1  # Redirect stderr to stdout for better logging

log_error() {
    echo "ERROR: $1" >&2
}

# for debugging
#set -x

###
#
# - Get all projects at all levels under CURATE_GROUP
# - For each project with more than MIN_RETAIN_LOGS logs
#   - Get its logs and order them by date (newest first)
#   - Keep an index, PIPELINE_INDX, of the logs as they are evaluated
#   - Skip any pipelines with an active pipeline
#   - Delete pipelines older than MAX_AGE_LOGS unless PIPELINE_INDX is less than MIN_RETAIN_LOGS
#   - Delete pipelines with a PIPELINE_INDX greater than MAX_RETAIN_LOGS unless the age of the pipeline is newer than MIN_AGE_LOGS
#
###

# CI/CD variables

# CURATE_BOT: name of the GitLab group member created along side the access token
# CURATE_TOKEN: the value of the GitLab group access token
# CURATE_GROUP: GitLab group to curate, applies to all projects at any level below this group, default the access token's group

# MIN_RETAIN_LOGS: keep at least this number of job logs regardless of age, default 10
# MAX_RETAIN_LOGS: purge job logs over this number as long as MIN_AGE_LOGS is preserved, default 100
# MIN_AGE_LOGS: keep all job logs created less than this number of days ago, default 7
# MAX_AGE_LOGS: purge job logs older than this number of days as long as MIN_RETAIN_LOGS is preserved, default 30

# Get all projects in GitLab visible to the access token stored in the CURATE_TOKEN variable

TOKEN="$CURATE_TOKEN"
GROUP="$CURATE_GROUP"
PER_PAGE=100
API_ENDPOINT="https://${CI_SERVER_FQDN}/api/v4"
GRAPHQL_ENDPOINT="https://${CI_SERVER_FQDN}/api/graphql"

source "${BASH_SOURCE%/*}/getCreds.sh"
source "${BASH_SOURCE%/*}/getProjects.sh"
source "${BASH_SOURCE%/*}/getPipelines.sh"
source "${BASH_SOURCE%/*}/pipelineAge.sh"

# get all projects subordiante to CURATE_GROUP and then for each project with at least MIN_RETAIN_LOGS pipelines
PROJECTS=$(getProjects | jq --slurp '.')
for i in $(echo "$PROJECTS" | jq --compact-output --argjson retain "${MIN_RETAIN_LOGS}" '.[] | select(.node.pipelineCounts.all > $retain) | {fullPath: .node.fullPath, id: .node.id, pipelineCount: .node.pipelineCounts.all}'); do

  PROJECT=$(echo "$i" | jq -r '.fullPath')
  PROJECT_ID=$(echo "$i" | jq -r '.id' | sed 's@^.*/@@')
  PIPELINE_COUNT=$(echo "$i" | jq -r '.pipelineCount')

  echo "PROJECT=$PROJECT"
  echo "PROJECT_ID=$PROJECT_ID"
  echo "PIPELINE_COUNT=$PIPELINE_COUNT"

  PIPELINE_INDX=0
  # get all pipelines for the project ordered by date (newest first)
  PIPELINES=$(getPipelines | jq --slurp '.')
  for j in $(echo "$PIPELINES" | jq 'sort_by(.node.updatedAt) | reverse' | jq --compact-output '.[] |  {id: .node.id, active: .node.active, updatedAt: .node.updatedAt}'); do

    PIPELINE_INDX=$((PIPELINE_INDX + 1))
    echo "PIPELINE_INDX=$PIPELINE_INDX"

    PIPELINE_ID=$(echo "$j" | jq -r '.id' | sed 's@^.*/@@')
    PIPELINE_ACTIVE=$(echo "$j" | jq -r '.active')
    PIPELINE_UPDATED_AT=$(echo "$j" | jq -r '.updatedAt')
    PIPELINE_AGE=$(pipelineAge "$PIPELINE_UPDATED_AT")

    echo "PIPELINE_ID=$PIPELINE_ID"
    echo "PIPELINE_ACTIVE=$PIPELINE_ACTIVE"
    echo "PIPELINE_UPDATED_AT=$PIPELINE_UPDATED_AT"
    echo "PIPELINE_AGE=$PIPELINE_AGE"

    # never delete active pipelines
    [[ "$PIPELINE_ACTIVE" == 'true' ]] && continue

    # delete pipelines older than MAX_AGE_LOGS unless PIPELINE_INDX is less than MIN_RETAIN_LOGS

    if [[ $PIPELINE_AGE -gt $MAX_AGE_LOGS && $PIPELINE_INDX -gt $MIN_RETAIN_LOGS ]]; then
      echo "Deleting pipeline $PIPELINE_ID due to age (index: $PIPELINE_INDX, age: $PIPELINE_AGE days)"
      getCreds | curl --request DELETE --fail --silent --show-error --location \
        --config - "${API_ENDPOINT}/projects/${PROJECT_ID}/pipelines/${PIPELINE_ID}"
      continue
    fi

    # delete pipelines with a PIPELINE_INDX greater than MAX_RETAIN_LOGS unless younger than MIN_AGE_LOGS

    if [[ $PIPELINE_INDX -gt $MAX_RETAIN_LOGS && $PIPELINE_AGE -gt $MIN_AGE_LOGS ]]; then
      echo "Deleting pipeline $PIPELINE_ID due to excessive number (index: $PIPELINE_INDX, age: $PIPELINE_AGE days)"
      getCreds | curl --request DELETE --fail --silent --show-error --location \
        --config - "${API_ENDPOINT}/projects/${PROJECT_ID}/pipelines/${PIPELINE_ID}"
      continue
    fi

  done

done
