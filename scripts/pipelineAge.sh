#!/usr/bin/env bash

pipelineAge() {

  local PIPELINE_UPDATED_AT PIPELINE_DATE PIPELINE_AGE CURRENT_TIME

  PIPELINE_UPDATED_AT=$1

  PIPELINE_DATE=$(date -d "$(echo $PIPELINE_UPDATED_AT | tr 'T' ' ' | tr -d 'Z')" +%s 2>/dev/null)

  if [ $? -ne 0 ]; then
    echo "Error: Invalid date format: $PIPELINE_UPDATED_AT"
    exit 1
  fi

  CURRENT_TIME=$(date +%s)

  if [ -n "$PIPELINE_DATE" ]; then
    PIPELINE_AGE=$(( (CURRENT_TIME - PIPELINE_DATE) / 86400 ))
  else
    echo "Error: PIPELINE_DATE is empty or not set"
    exit 1
  fi

  echo "$PIPELINE_AGE"

}
