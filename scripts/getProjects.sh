#!/usr/bin/env bash

getProjects() {

  ## Global variables required
  #
  # TOKEN
  # GROUP
  # PER_PAGE
  # GRAPHQL_ENDPOINT

  ## Global functions required
  #
  # GetCreds

  local AFTER QUERY

  read -r -d '' QUERY <<'EOF'
query($fullPath: ID!, $first: Int, $after: String) {
  group(fullPath: $fullPath) {
    projects(includeSubgroups: true, first: $first, after: $after) {
      count
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          name
          fullPath
          id
          pipelineCounts {
            all
          }
        }
      }
    }
  }
}
EOF

  AFTER=null

  while :; do

    local VARIABLES PAYLOAD RESPONSE HAS_NEXT_PAGE END_CURSOR

    read -r -d '' VARIABLES <<EOF
{
  "fullPath": "${GROUP}",
  "first": ${PER_PAGE},
  "after": ${AFTER}
}
EOF

    PAYLOAD=$(
      jq --null-input --arg query "$QUERY" --argjson variables "$VARIABLES" \
        '{"query": $query, "variables": $variables}'
    )

    RESPONSE=$(
      getCreds |
        curl --request POST --fail --silent --show-error --location \
          --header "Content-Type: application/json; charset=utf-8" \
          --config - "${GRAPHQL_ENDPOINT}" --data "$PAYLOAD"
    )

    HAS_NEXT_PAGE=$(echo "$RESPONSE" | jq -r '.data.group.projects.pageInfo.hasNextPage')
    END_CURSOR=$(echo "$RESPONSE" | jq -r '.data.group.projects.pageInfo.endCursor')
    echo "$RESPONSE" | jq -r '.data.group.projects.edges[] | @json'

    if [[ "$HAS_NEXT_PAGE" == 'false' ]]; then break; fi

    read -r -d '' AFTER <<EOF
"${END_CURSOR}"
EOF

  done

}
